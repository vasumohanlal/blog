from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from views import PortalView
from blog_api import PortalViewSet


portal_list_create = PortalViewSet.as_view({
    'get': 'list',
    'post': 'create'
})

urlpatterns = [
    url(r'^portal/$', portal_list_create, name='List Messages'),
    url(r'^post/$', PortalView.as_view(), name='Resume Portal'),
]

urlpatterns = format_suffix_patterns(urlpatterns)
