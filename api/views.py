from django.shortcuts import render

# Create your views here.
from django.views.generic import FormView
from forms import PortalForm
from models import Portal


class PortalView(FormView):
    form_class = PortalForm
    template_name = 'portal.html'

    def get_all_default_topics(self):
        # TODO write the proper query
        return Portal.objects.all()

    def get_context_data(self, **kwargs):
        """
        Insert the form into the context dict.
        """
        if 'form' not in kwargs:
            kwargs['form'] = self.get_form()
            kwargs['default_topics'] = self.get_all_default_topics()
        return super(PortalView, self).get_context_data(**kwargs)
