from django.contrib import admin

# Register your models here.
from .models import Portal
from import_export.admin import ImportExportActionModelAdmin


class PortalAdmin(ImportExportActionModelAdmin, admin.ModelAdmin):
    search_fields = ['name', 'contact_number', 'experience', 'resume', 'status']
    list_display = ['name', 'contact_number', 'experience', 'resume', 'status']
    list_filter = ['status']

    class Meta:
        model = Portal

admin.site.register(Portal, PortalAdmin)
