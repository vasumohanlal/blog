from rest_framework import serializers
from api.models import Portal
from .constants import *
import logging

logger = logging.getLogger(__name__)


class ApiSerializer(serializers.Serializer):
    """
    Validates the request from the json and response --
    name
    contact_number
    experience
    resume
    is_accepted
    is_rejected
    """
    name = serializers.CharField(max_length=255, required=True, help_text="Enter your name in capital")
    contact_number = serializers.CharField(max_length=255, default=0, required=False, help_text="+919744XXXXXXX")
    experience = serializers.ChoiceField(choices=EXP_TYPE, default=FRESHER, help_text="Enter your experience")
    resume = serializers.FileField(max_length=None, allow_empty_file=False, use_url=True,
                                   help_text="upload your resume .docx format only")
    status = serializers.ChoiceField(choices=status, default=on_hold, help_text="applicant status")

    def create(self, validated_data):
        """
        Create and return a new `Resume record` instance, given the validated data.
        """
        new_obj = Portal()
        new_obj.name = self.initial_data.get('name')
        new_obj.contact_number = self.initial_data.get('contact_number')
        new_obj.experience = self.initial_data.get('experience')
        new_obj.resume = self.initial_data.get('resume')
        new_obj.status = self.initial_data.get('status', 3)
        new_obj.save()
        return new_obj
